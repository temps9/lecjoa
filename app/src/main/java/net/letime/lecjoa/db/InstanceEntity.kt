/* Copyright 2018 Conny Duck
 *
 * This file is a part of lecjoa.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * lecjoa is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with lecjoa; if not,
 * see <http://www.gnu.org/licenses>. */

package net.letime.lecjoa.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverter
import android.arch.persistence.room.TypeConverters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import net.letime.lecjoa.entity.Emoji

@Entity
@TypeConverters(Converters::class)
data class InstanceEntity(
        @field:PrimaryKey var instance: String,
        val emojiList: List<Emoji>?,
        val maximumTootCharacters: Int?)


class Converters {

    @TypeConverter
    fun jsonToList(emojiListJson: String?): List<Emoji>? {
        return Gson().fromJson(emojiListJson, object : TypeToken<List<Emoji>>() {}.type)
    }

    @TypeConverter
    fun listToJson(emojiList: List<Emoji>?): String {
        return Gson().toJson(emojiList)
    }
}