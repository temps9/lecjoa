[![Translate - with Stringlate](https://img.shields.io/badge/translate%20with-stringlate-green.svg)](https://lonamiwebs.github.io/stringlate/translate?git=https%3A%2F%2Fgithub.com%2Flecjoaapp%2Flecjoa) [![Build Status](https://www.bitrise.io/app/55b2f0c77c4bba74/status.svg?token=elUl9fieM5K34iLRL0rpoA&branch=master)](https://www.bitrise.io/app/55b2f0c77c4bba74) [![CircleCI](https://circleci.com/gh/lecjoaapp/lecjoa.svg?style=svg)](https://circleci.com/gh/lecjoaapp/lecjoa)
# lecjoa

![](/fastlane/metadata/android/en-US/images/icon.png)

lecjoa is a beautiful Android client for [Mastodon](https://github.com/tootsuite/mastodon). Mastodon is a GNU social-compatible federated social network. That means not one entity controls the whole network, rather, like e-mail, volunteers and organisations operate their own independent servers, users from which can all interact with each other seamlessly.

[<img src="/assets/fdroid_badge.png" alt="Get it on F-Droid" height="80" />](https://f-droid.org/repository/browse/?fdid=net.letime.lecjoa)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" alt="Get it on Google Play" height="80" />](https://play.google.com/store/apps/details?id=net.letime.lecjoa&utm_source=github&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1)
[<img src="/assets/amazon_badge.png" alt="Get it on Amazon" height="80" />](https://www.amazon.com/dp/B077ZWWX9T)

## Features

- Material Design
- Most Mastodon APIs implemented
- Multi-Account support
- Dark and light theme with the possibility to auto-switch based on the time of day
- Drafts - compose toots and save them for later
- Choose between different emoji styles 
- Optimized for all screen sizes
- Completely open-source - no non-free dependencies like Google services

### Support

If you have any bug reports, feature requests or questions please open an issue or send us a toot at [lecjoa@mastodon.social](https://mastodon.social/@lecjoa)!

### Head of development

This app was developed by [temps9@mastodon.social](https://mastodon.social/users/temps9).
The current maintainer is [temps9@mastodon.social](https://mastodon.social/users/temps9).

### Development chatroom
https://riot.im/app/#/room/#lecjoa:matrix.org
