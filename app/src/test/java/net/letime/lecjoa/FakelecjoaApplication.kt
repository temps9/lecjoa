package net.letime.lecjoa

/**
 * Created by charlag on 3/7/18.
 */

class FakelecjoaApplication : lecjoaApplication() {

    lateinit var locator: ServiceLocator

    override fun initAppInjector() {
        // No-op
    }

    override fun initPicasso() {
        // No-op
    }

    override fun getServiceLocator(): ServiceLocator {
        return locator
    }
}